# IO UML

Projekt zaliczeniowy na przedmiot Inżynieria Oprogramowania rok akademicki 2018/19

Projekt dotyczy aplikacji do zarządzania sklepem z częściami komputerowymi. W jego skład wchodzą:
* diagram klas,
* diagram obiektów,
* diagram przypadków użycia,
* diagram przebiegu,
* diagram kooperacji,
* diagram stanu,
* diagram czynności,
* diagram komponentów,
* diagram wdrożenia.